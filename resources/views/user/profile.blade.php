@extends('user.temp_basic')
@section('judul_halaman', 'Profile')
@section('content')
<!-- Start employeeslist Area -->
<div class="row mt-5 mb-5">
  <div class="col-md-12">
    <div class="card" style="width: 48.5em;margin:0 auto;">
      <div class="card-header text-center">
        <h2>Profile</h2>
      </div>
      <div class="card-body">
        <div class="container">
          <div class="row my-2">
            <div class="col-lg-12 order-lg-2">
              <ul class="nav nav-tabs">
                <li class="nav-item">
                  <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Profile</a>
                </li>

                <li class="nav-item">
                  <a href="" data-target="#edit" data-toggle="tab" class="nav-link">Edit</a>
                </li>
                <li class="nav-item">
                  <a href="" data-target="#upload" data-toggle="tab" class="nav-link">Upload Pembayaran</a>
                </li>

              </ul>
              <div class="tab-content py-4">
                <div class="tab-pane active" id="profile">
                  <h5 class="mb-3 text-center">User Profile</h5>
                  <div class="row">
                    <dl class="row">


                      <dt class="col-sm-5">Nama</dt>
                      <dd class="col-sm-7">
                        @if(!empty(Auth::user()->name))
                        {{Auth::user()->name}}
                        @else
                        data anda belum terisi
                        @endif
                      </dd>

                      <dt class="col-sm-5">Email</dt>
                      <dd class="col-sm-7">  @if(!empty(Auth::user()->email))
                        {{Auth::user()->email}}
                        @else
                        data anda belum terisi
                        @endif</dd>


                        <dt class="col-sm-5">Daftar Transaksi</dt>
                        <table class="table table-dark ml-5 mt-1 mr-5">
                          <thead>
                            <tr>
                              <th scope="col">No</th>
                              <th scope="col">Tgl Order</th>
                              <th scope="col">Jumlah Tiket</th>
                              <th scope="col">Tempat Wisata</th>
                              <th scope="col">Tgl Tiket</th>
                              <th scope="col">Harga</th>
                              <th scope="col">Total</th>
                              <th scope="col">Status</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th scope="row">1</th>
                              <td>Mark</td>
                              <td>Otto</td>
                              <td>@mdo</td>
                              <td>@mdo</td>
                              <td>@mdo</td>
                              <td>@mdo</td>
                              <td>Belum dibayar</td>
                            </tr>
                          </tbody>
                        </table>

                        <div class="alert alert-primary ml-5 mr-5" role="alert">
                          Status belum dibayar silahkan upload bukti transfer anda !
                        </div>
                      </dl>
                    </div>
                  </div>

                  <div class="tab-pane" id="edit">
                    <form method="POST" enctype="multipart/form-data" type="hidden" name="_method" value="PUT" action="profile/edit">
                      {{ csrf_field() }}
                      <input type="hidden" name="user_id" value=""> <br/>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label form-control-label">Nama</label>
                        <div class="col-lg-9">
                          <input class="form-control" type="text" value="" name="name">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label form-control-label">Email</label>
                        <div class="col-lg-9">
                          <input class="form-control" type="email" value="" name="email">
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label form-control-label"></label>
                        <div class="col-lg-9">
                          <input type="submit" class="btn btn-primary" value="Save Changes">
                        </div>
                      </div>
                    </form>
                  </div>


                  <div class="tab-pane" id="upload">
                    <h5 class="mb-3 text-center">Upload Bukti Pembayaran</h5>
                    <form method="POST" enctype="multipart/form-data" type="hidden" name="_method" value="PUT" action="profile/edit">
                      {{ csrf_field() }}
                      <input type="hidden" name="user_id" value=""> <br/>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label form-control-label">Ubah Foto</label>
                        <div class="col-lg-9">
                          <input type="file" class="form-control"  name='avatar'>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-lg-3 col-form-label form-control-label"></label>
                        <div class="col-lg-9">
                          <input type="submit" class="btn btn-primary" value="Save Changes">
                        </div>
                      </div>
                    </form>
                  </div>




                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
